# NSI Épreuves Pratiques 

Site (temporaire) pour centraliser les retours, commentaires sur les épreuves pratiques en spécialité NSI.

Les discussions se trouvent sur la discussion [BNS, d'autres erreurs encore non pointées ?](https://mooc-forums.inria.fr/moocnsi/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745) du Forum NSI.

La présentation s'inspire du travail de Gilles Lassus : [BNS 2022](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2022/)

_travail en cours_

État d'avancement :

- Sujets 1 à 5 : traités
- Sujets 6 à 10 : traités
- Sujets 11 à 15 : traités
- Sujets 16 à 20 : traités
- Sujets 21 à 25 : non traités
- Sujets 26 à 30 : non traités
- Sujets 31 à 35 : non traités
- Sujets 36 à 40 : _en cours_ (merci à R. janvier pour le coup de main)


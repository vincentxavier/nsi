Q. Konieczko remarque l'erreur grossière de la valeur par défaut sur un paramètre mutable. Il pointe la lourdeur de la notation pointée en cascade : `self.racine.gauche.insere(element)` 

Source : [commentaires à propos du 17.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/12).


On dispose d’un programme permettant de créer un objet de type `PaquetDeCarte`,
selon les éléments indiqués dans le code ci-dessous.
Compléter ce code aux endroits indiqués par `# A compléter`, puis vérifier que vous obtenez les bonnes sorties sur les tests proposés.

```python linenums='1'
VALEURS = ['', 'As', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Valet', 'Dame', 'Roi']
COULEURS = ['', 'pique', 'coeur', 'carreau', 'trefle']

class Carte:
    """Initialise couleur (de 1 à 4), et valeur (de 1 à 13)"""

    def __init__(self, couleur, valeur):
        self.couleur = couleur
        self.valeur = valeur

    def get_nom(self):
        """Renvoie le nom de la Carte As, 2, ... 10, Valet, Dame, Roi"""
        return VALEURS[self.valeur]

    def get_couleur(self):
        """Renvoie la couleur de la Carte (parmi pique, coeur, carreau, trefle)"""
        return COULEURS[self.couleur]

class PaquetDeCarte:
    """Initialise un paquet de cartes, avec un attribut contenu, de type list, vide"""

    def __init__(self):
        # A compléter

    def remplir(self):
        """Remplit le paquet de 52 cartes : en parcourant les couleurs puis les valeurs"""
        # A compléter

    def get_carte_at(self, pos):
        """Renvoie la Carte qui se trouve à la position donnée ou None si ce n'est pas possible"""
        # A compléter
```

Exemple :

```python
>>> jeu = PaquetDeCarte()
>>> jeu.remplir()
>>> carte = jeu.get_carte_at(20)
>>> print(carte.get_nom() + " de " + carte.get_couleur())
8 de coeur
>>> autre = jeu.get_carte_at(0)
>>> print(autre.get_nom() + " de " + autre.get_couleur())
As de pique
>>> une_derniere = jeu.get_carte_at(51)
>>> print(une_derniere.get_nom() + " de " + une_derniere.get_couleur())
Roi de trefle
```
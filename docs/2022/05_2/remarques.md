G. Connan pointe du doigt toutes les erreurs du premier sujet utilisant la POO : [commentaires à propos du 05.2](https://gitlab.com/ens-fr/nsi-pratique.git). Cet exercice est à reprendre en profondeur.

L'exemple donné ne peut être reproduit quelque soit la façon (simple) dont on constitue le paquet. La réponse la plus proche en rangeant par couleur puis par valeur est `8 de pique`.

Dans la proposition d'énoncé modifié :

- on corrigera les erreurs PEP8 de nommage et d'espaces, 
- on utilisera des constantes pour les noms de valeurs et ceux des couleurs, en insérant dans ces listes un premier élément _bidon_ pour tenir compte des valeurs et couleurs qui commencent à 1, 
- on essaiera de rajouter une question pour que l'élève complète la création d'un attribut d'instance, histoire de faire un petit peu de POO.
- on corrigera le test faux et on en ajoutera
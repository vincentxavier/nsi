```python linenums='1'
VALEURS = (5, 2, 1)

def rendu(somme_a_rendre):
    retour = [0, 0, 0]
    reste_a_rendre = somme_a_rendre
    for i in range(3):
        retour[i] = reste_a_rendre // VALEURS[i]
        reste_a_rendre = reste_a_rendre % VALEURS[i]
    return retour
```

On peut se servir de la fonction `divmod` si on la connait :

```python linenums='1'
VALEURS = (5, 2, 1)

def rendu(somme_a_rendre):
    retour = [0, 0, 0]
    for i in range(3):
        retour[i], somme_a_rendre = divmod(somme_a_rendre, VALEURS[i])
    return retour
```

Certains candidats (la plupart ?) ne sauront pas ou ne penserons pas à utiliser une liste (3 ce n'est pas beaucoup) et feront quelque chose comme :

```python
def rendu(somme_a_rendre):
    n5 = somme_a_rendre // 5
    somme_a_rendre = somme_a_rendre % 5
    n2 = somme_a_rendre // 2
    n1 = somme_a_rendre % 2
    return [n5, n2, n1]
```
On s’intéresse au problème du rendu de monnaie. On suppose qu’on dispose d’un nombre infini de billets de 5 euros, de pièces de 2 euros et de pièces de 1 euro.

Le but est d’écrire une fonction nommée `rendu` dont le paramètre est un entier positif non nul `somme_a_rendre` et qui retourne une liste de trois entiers correspondent (dans cet ordre) aux nombres de billets de 5 euros, de pièces de 2 euros et de pièces de 1 euro à rendre afin que le total rendu soit égal à `somme_a_rendre`.

On commencera par rendre le nombre maximal de billets de 5 euros, puis celui des pièces de 2 euros et enfin celui des pièces de 1 euros.

Vous pourrez utiliser la constante suivante :

```python
VALEURS = (5, 2, 1)
```

Exemples :

```python
>>> rendu(13)
[2,1,1]
>>> rendu(64)
[12,2,0]
>>> rendu(89)
[17,2,0]
>>> rendu(4)
[0, 2, 0]
```
R. Janvier pense (à raison) que l'exercice du rendu de monnaie semble un peu difficile pour un exercice 1, même si l'algorithme est l'une des stars du programme de NSI. Il poursuit sur quelques choix discutables sur la forme. Retrouvez son analyse complète : [commentaires à propos du 13.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/19) 

Sa proposition de ne pas insister sur le fait qu'il s'agisse d'un algorithme glouton semble raisonnable et c'est sa version que nous mettons à disposition dans la version modifiée de l'énoncé.

Peut-être ne pas mettre dans l'énoncé de nom pour les trois variables de la liste résultat puisque suivant la solution adoptée par le candidat, ces variables peuvent ne pas être explicites.

De plus, puisque le système monétaire est figé, il serait souhaitable de le proposer en constante :

```python
VALEURS = (5, 2, 1)
```
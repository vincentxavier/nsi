L'opérateur « ou exclusif » (parfois représenté par le symbole ⊕) entre deux bits renvoie 0 si les deux bits sont égaux et 1 s'ils sont
différents. Ci-dessous la table de vérité de cet opérateur.  

|   a   |   b   | a ⊕ b |
| :---: | :---: | :---: |
|   0   |   0   |   0   |
|   0   |   1   |   1   |
|   1   |   0   |   1   |
|   1   |   1   |   0   |

Écrire la fonction ```xor``` qui prend en paramètres deux tableaux de bits de même longueur et qui renvoie un tableau où l’élément situé à la position `i` est le résultat, par l’opérateur  « ou exclusif » , des
éléments à la position `i` des tableaux passés en paramètres.


Exemples :

```python
>>> a = [1, 0, 1, 0, 1, 1, 0, 1]
>>> b = [0, 1, 1, 1, 0, 1, 0, 0]
>>> c = [1, 1, 0, 1]
>>> d = [0, 0, 1, 1]
```

En considérant les quatre exemples ci-dessus, cette fonction doit passer les tests suivants :

```python
>>> xor(a, b)
[1, 1, 0, 1, 1, 0, 0, 1]
>>> xor(c, d)
[1, 1, 1, 0]
```
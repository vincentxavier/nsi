Écrire une fonction python `nb_repetitions` qui prend en paramètres un
élément `element` et un tableau `tab` et renvoie le nombre de fois où l’élément apparaît dans le tableau.

Exemples :

```python
>>> nb_repetitions(5, [2, 5, 3, 5, 6, 9, 5])
3
>>> nb_repetitions('A', ['B', 'A', 'B', 'A', 'R'])
2
>>> nb_repetitions(12, [1, 8 , 7, 21, 36, 44])
0
```
```python linenums='1'
def nb_repetitions(element, tab):
    nb = 0
    for elt in tab:
        if elt == element:
            nb += 1
    return nb
```

L'examinateur devra veiller à ce que le candidat ne se contente pas de :

```python
def nb_repetitions(element, tab):
    return tab.count(element)
``` 

La fonction `sum`  peut prendre un itérateur en paramètre, et _caster_ correctement les booléens (`True`  vaudra 1, ` False` 0) pour offrir une autre solution en 1 ligne :

```python
def nb_repetitions(element, tab):
    return sum(elt == element for elt in tab)
```


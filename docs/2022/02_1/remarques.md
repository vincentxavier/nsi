Pas grand chose à dire sur cet exercice. Je ne sais pas si une moyenne pondérée fait vraiment partie des algorithmes classiques censé avoir été étudié en classe... de NSI, pas de matématiques ;-).

G. Connan qui a relu ce sujet s'interroge sur les attendus :

> Pas grand chose  à dire, à part que  c'est plus facile à écrire qu'un  tri ou un
algo glouton comme on en demande dans d'autres sujets.
Attend-on une gestion de la liste vide, des coefficients négatifs ?

Source : [commentaires à propos du 02.1](https://gitlab.com/nsind/tale/-/blob/master/docs/DIVERS/bns22.md#exercice-21)

Il a été suggéré de préciser **strictement positif** pour les coefficients.
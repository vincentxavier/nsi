Avec les notations de l'énoncé corrigé :

```python linenums='1'
def pascal(n):
    triangle = [[1]]
    for k in range(1, n+1):
        tab_k = [[1]]
        for i in range(1, k):
            tab_k.append(triangle[k-1][i-1] + triangle[k-1][i])
        tab_k.append(1)
        triangle.append(tab_k)
    return triangle
```
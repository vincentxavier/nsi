La fonction `fusionne` prend en paramètres `l1` et `l2` deux listes d’entiers triées par ordre croissant et les fusionne en une liste triée `l12` qu’elle renvoie.

Le code Python de la fonction est

```python linenums='1'
def fusionne(l1, l2):
    n1 = len(l1)
    n2 = len(l2)
    l12 = []
    i1 = 0
    i2 = 0
    while i1 < n1 and ... :
        if l1[i1] < ...:
            l12.append(...)
            i1 ...
        else:
            l12.append(...)
            i2 ...
    while i1 < n1:
        l12.append(...)
        i1 ...
    while ...:
        ...
        ...
    return l12
```

Compléter le code.

Exemples :

```python
>>> fusionne([1, 6, 10], [0, 7, 8, 9])
[0, 1, 6, 7, 8, 9, 10]
>>> fusionne([], [0, 7, 8, 9])
[0, 7, 8, 9]
>>> fusionne([1, 6, 10], [])
[1, 6, 10]
``` 
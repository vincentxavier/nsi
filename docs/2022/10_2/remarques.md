F. Chambon relève encore des soucis PEP8 (mauvais nommage et espaces manquantes). Il dénonce l'initialisation du tableau résultat par concaténation multiple : 

```python
tableau = [0] * n
``` 

devrait être remplacé par :

```python
tableau = [0 for _ in range(n)]
```

Je ne suis pas d'accord avec ça. Le seul reproche qu'on peut faire à `[0] * n` c'est que les élèves pourraient vouloir s'en servir pour initialiser une liste de listes.

Mais pour une liste d'objets non mutables (donc une liste d'entiers) l'écriture `[0] * n` est quand beaucoup plus concise, lisible et... efficace en temps (un rapport 10 environ)

Par contre je suis d'accord avec la remarque sur le fait que le résultat final peut être une liste qu'on va remplir avec des `append` successifs.
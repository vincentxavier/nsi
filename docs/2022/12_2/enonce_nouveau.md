On considère un tableau d'entiers `tab` (type `list` dont les éléments sont des `0` ou des `1`). On se propose de trier ce tableau selon l'algorithme suivant : à chaque étape du tri, le tableau est constitué de trois zones consécutives, la première ne contenant que des `0`,
la seconde n'étant pas triée et la dernière ne contenant que des `1`.

<table>
<tr>
<td>Zone de 0</td><td>Zone non triée</td><td>Zone de 1</td>
</tr>
</table>

Tant que la zone non triée n'est pas réduite à un seul élément, on regarde son premier
élément :

- si cet élément vaut 0, on considère qu'il appartient désormais à la zone ne contenant
que des 0 ;
- si cet élément vaut 1, il est échangé avec le dernier élément de la zone non triée et on
considère alors qu’il appartient à la zone ne contenant que des 1.

Dans tous les cas, la longueur de la zone non triée diminue de 1.

Compléter les fonctions `echange` et `separe` :

```python linenums='1'
def echange(tab, i, j):
    """echange les valeurs du tableau tab se trouvant aux indices i et j"""
    ...


def separe(tab):
    """place tous les 0 de tab à gauche et tous les 1 à droite"""
    debut = ...  # indice de début
    fin = ...    # indice de fin
    while debut < fin:
        if tab[debut] == 0:
            debut = ...
        else:
            ...
            fin = ...
```

Exemple :
```python
>>> L = [8, 8, 1, 8, 8, 0, 8]
>>> echange(L, 2, 5)
>>> L
[8, 8, 0, 8, 8, 1, 8]
>>> T = [0, 1, 0, 1, 0, 1, 0, 1, 0]
>>> separe(T)
>>> T
[0, 0, 0, 0, 0, 1, 1, 1, 1]       
```
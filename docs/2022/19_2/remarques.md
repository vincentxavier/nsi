Q. Konieczko est assez indulgent avec cet exercice : [commentaires à propos du 19.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/12). Il pointe les erreurs de non respect du PEP8 et s'étonne de la gestion de l'erreur par un `print`. 

Effectivement les exceptions ne sont pas au programme. Mais la gestion des bornes de l'intervalle de recherche dans une recherche dichotomique doit-elle être exposée ? Je crois plutôt que cette recherche doit se faire à l'aide de 2 fonctions dont l'une n'expose pas ces paramètres à l'utilisateur :

```python
def recherche(tableau, element):
    return recherche_dichotomique(tableau, element, 0, len(tableau)-1)
```

Dans ces conditions, il n'y a pas d'erreur possible sur les bornes. Dans l'énoncé peut-être ne faut-il pas faire apparaître le terme dichotomique qui pourrait être une question de l'examinateur.
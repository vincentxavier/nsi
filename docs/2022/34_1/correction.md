Version attendue :

```python linenums='1'
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
            'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
            's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

def occurrence_max(chaine):
    occurence = [0] *  26
    for i in range(26):
        compteur = 0
        for caractere in chaine:
            if caractere == alphabet[i]:
                compteur = compteur + 1
        occurence[i] = compteur
    ind_max = 0
    for i in range(26):
        if occurence[i] > occurence[ind_max]:
            ind_max = i
    return alphabet[ind_max]
```

Version avec `index` :
```python linenums='1'
def occurence_max(texte):
    occurence = [0] * 26
    for s in texte:
        if s in alphabet:
            occurence[alphabet.index(s)] += 1
    m = 0
    lettre = ""
    for i in range(26):
        if occurence[i] > m:
            m = occurence[i]
            lettre = alphabet[i]
    return lettre
```

Version avec un dictionnaire :
```python linenums='1'
def occurrence_max(chaine):
    occurrences = {lettre: 0 for lettre in alphabet}
    lettre_max = 'a'
    for caractere in chaine:
        if caractere in occurrences:
            occurrences[caractere] += 1
            if occurrences[caractere] > occurrences[lettre_max]:
                lettre_max = caractere
    return lettre_max
```


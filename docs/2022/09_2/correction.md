```python linenums='1'
CODE_LETTRE = {"A":1, "B":2, "C":3, "D":4, "E":5, "F":6, "G":7,
                "H":8, "I":9, "J":10, "K":11, "L":12, "M":13, 
                "N":14, "O":15, "P":16, "Q":17, "R":18, "S":19, 
                "T":20, "U":21,"V":22, "W":23, "X":24, "Y":25, "Z":26}

def est_parfait(mot):
    """mot est une chaîne de caractères (en lettres majuscules) la fonction
    calcule les codes concatenés et additionnés, renvoie un triplet constitué
    de ces codes ainsi que du booléen qui vaudra True ssi le mot est parfait"""
    str_code_concatene = ""
    code_additionne = 0
    for lettre in mot:
        code_lettre = CODE_LETTRE[lettre]
        str_code_concatene = str_code_concatene + str(code_lettre)
        code_additionne += code_lettre 
    code_concatene = int(str_code_concatene)
    mot_est_parfait = code_concatene % code_additionne == 0
    return code_additionne, code_concatene, mot_est_parfait
```

F. Chambon relève pas d'améliorations à apporter à l'énoncé vis-à-vis du PEP8 : espaces, noms des variables. Il pense également que le dictionnaire n'est pas nécessaire ici pour coder le décalage, un simple calcul à l'aide de la fonction `ord` suffit. Je ne sais pas trop pour cette remarque.

Un dictionnaire est plus souple d'utilisation que le calcul via `ord`. De plsu dans le sujet ça oblige à introduire une nouvelle fonction et donc l'expliquer. Je resterais donc sur le dictionnaire pour le code d'une lettre.

Enfin sa relecture se termine par un cri du coeur sur :

```python
code_c = int(code_c)
``` 

Source : [commentaires à propos du 09.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/22)

D'un point de vue analyse de code il est certain que changer les types des objets référencés par une variable n'est pas génial. Mais en Python ça n'est pas vraiment génant et surtout, les pratiques de juges en ligne font qu'on a tendance à le faire souvent.

Par exemple quand on récupère des données de type `str`, `str` et `int` :

```python
prenom, nom, age = input().split()
age = int(age)
```

On n'a pas envie pour chacune des variables entières devoir manipuler le double de variables. Dans le cadre d'une sujet de BAC toutefois, on peut faire attention et proposer :

```python
code_concatene = int(str_code_concatene)
```

Enfin dernières remarques : 
- une fonction retourne plusieurs valeurs grâce à un tuple ; pas besoin d'une liste
- le `if` est superflu.
G. Connan nous propose la fonction probablement attendue :

```python linenums='1'
def consecutifs_successifs(tab):
    couples = []
    for i in range(len(tab) - 1):
        if tab[i+1] - tab[i] == 1:
            couples.append((tab[i], tab[i+1]))
    return couples
```

Peut-être certain-es feront une version en compréhension :

```python
def consecutifs_successifs(tab):
    return [(tab[i], tab[i+1]) for i in range(len(tab) - 1) if tab[i+1] - tab[i] == 1]
```
Q. Konieczko note quelques petites erreurs de typographie. 

L'exercice est un des rares à mettre l'accent sur l'interaction de plusieurs fonctions pour résoudre un problème. Ici, trois fonctions au total, mais, très très courtes et finalement l'élève n'a qu'à compléter les appels manquants. Cela semble un peu léger. 

L'autre souci c'est que l'algorithme sous-jacent pour tester qu'un mot est palindrome est naïf : inverser l'ordre des lettres et vérifier l'égalité avec le mot original. Ce qui en terme de complexité fait de l'ordre de $2n$ manipulations (si $n$ est la longueur du mot) contre $n/2$ si on applique l'algorithme classique consistant à tester les lettres en partant de chacune des extrémités.

On peut garder le sujet ainsi, mais c'est un exercice vraiment facile. Pas d'énoncé modifié à proposer.
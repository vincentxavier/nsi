G. Connan est assez dur avec cet exercice : [commentaires à propos du 04.2](https://gitlab.com/nsind/tale/-/blob/master/docs/DIVERS/bns22.md#exercice-42).

Je suis plus nuancé. J'imagine que l'intérêt est essentiellement de manipuler la récursivité sur un exemple non trivial, et que cette propagation sur un bloc peut se retrouver dans certaines opérations sur les images. 

Par contre ce que je changerai dans l'énoncé c'est le code de la fonction :

- ajouter un peu plus de trous progressivement tout au long des 4 appels récursifs qui se ressemblent énormément
- revoir le début : pourquoi ce test avec un `return None`  qui ne rime à rien
- on peut aussi retirer tout ce paranthésage qui alourdit le texte
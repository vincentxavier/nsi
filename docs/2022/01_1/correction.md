Dans ce corrigé on adopte le nom de l'énoncé corrigé :

```python linenums='1'
def nb_occurrences(caractere, mot):
    nb_occ = 0
    for lettre in mot:
        if lettre == caractere:
            nb_occ += 1
    return nb_occ
```

Version avec parcourt par les indices, qui n'a aucun intérêt :

```python linenums='1'
def nb_occurrences(caractere, mot):
    nb_occ = 0
    for i in range(len(mot)):
        if mot[i] == caractere:
            nb_occ += 1
    return nb_occ
```

Version **hors programme** qui utilise le _cast_ automatique de booléen vers _int_

```python linenums='1'
def nb_occurrences(caractere, mot):
    return sum(lettre == caractere for lettre in mot)
```

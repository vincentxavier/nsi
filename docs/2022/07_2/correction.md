```python linenums='1'
def echange(tab, i, j):
    tab[i], tab[j] = tab[j], tab[i]

def tri_bulles(tab):
    n = len(tab)
    for i in range(n-1, -1, -1):
        for j in range(i):
            if tab[j] > tab[j+1]:
                echange(tab, j, j+1)
```


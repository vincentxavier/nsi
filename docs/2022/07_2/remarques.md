F. Chambon rappelle que le tri à bulles, thème de cet exerice 2 est hors-programme. Ce n'est pas forcément génant en exercice 2 puisqu'une partie du code est fournie. Il faut néanmoins une explication de l'algorithme qu'implémente ce code.

Source [commentaires à propos du 07.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/22)

Dans l'énoncé modifié, une tentative d'explication courte... pas sûr que ça aide un élève. Autre modification : lorsqu'on a besoin d'échanger deux valeurs dans un tableau, faire une fonction `echange` à trois paramètres et laisser le candidat coder son échange (soit avec une affectation multiple soit en utilisant une 3e variable comme certains le font).
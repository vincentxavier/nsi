On considère des mots à trous : ce sont des chaînes de caractères contenant uniquement
des majuscules et des caractères `*`. Par exemple `INFO*MA*IQUE`, `***I***E**` et
`*S*` sont des mots à trous. 

Écrire une fonction `correspond` prenant deux chaînes de caractères paramètres : `mot` et `mot_a_trous` où `mot_a_trous` est un mot à trous comme indiqué ci-dessus. La fonction renvoie `True` si on peut obtenir `mot` en remplaçant convenablement les caractères `'*'` de `mot_a_trous`, `False` sinon. Il est entendu que le caractère `'*'` remplace une et une seule lettre majuscule. 


Exemples :

```python
>>> correspond('INFORMATIQUE', 'INFO*MA*IQUE')
True
>>> correspond('AUTOMATIQUE', 'INFO*MA*IQUE')
False
```
```python
def correspond(mot, mot_a_trous):
    for i in range(len(mot)):
        if mot_a_trous[i] != '*' and mot_a_trous[i] != mot[i]:
            return False
    return True
```

Versions complètement **hors-programme**

L'occasion de (re-)découvrir les fonctions `all` et `any` ...avec le bonus d'illustrer :

$$\neg\left(\exists x\ A(x) \wedge B(x)\right) \Leftrightarrow \forall x\ \neg A(x) \vee \neg B(x)$$

On commence par le `any`, traduction directe de la version précédente.

```python
def correspond(mot, mot_a_trous):
    return not any(mot_a_trous[i] != '*' and mot_a_trous[i] != mot[i] for i in range(len(mot)))
```

```python
def correspond(mot, mot_a_trous):
    return all(mot_a_trous[i] == '*' or mot_a_trous[i] == mot[i] for i in range(len(mot)))
```

Ou avec un `enumerate` si on n'aime pas `range` :

```python
def correspond(mot, mot_a_trous):
    return all(mot_a_trous[i] == '*' or mot_a_trous[i] == lettre for i, lettre in enumerate(mot))
```

Écrire une fonction `valeur_et_indice_max` qui prend en paramètre un tableau `tab` non vide de nombres entiers et renvoie un couple donnant le plus grand élément de cette liste, ainsi que l’indice de la première apparition de ce maximum dans la liste.

Exemples :
```python
>>> valeur_et_indice_max([1, 5, 6, 9, 1, 2, 3, 7, 9, 8])
(9, 3)
>>> valeur_et_indice_max([1, 1, 1, 4, 4])
(4, 3)
>>> valeur_et_indice_max([10])
(10, 0)
```
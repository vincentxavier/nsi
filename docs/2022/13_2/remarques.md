R. Janvier tente de sauver cet exercice et vous pouvez jeter un oeil à son analyse : [commentaires à propos du 13.2]

Mais cet exercice est à revoir entièrement : l'implémentation de la structure de File avec une seule entrée et un chaînage renversé (ie dans le sens inverse de ce qu'il faudrait faire pour avoir des opérations efficaces) est une hérésie, j'explique pourquoi ici : [à propos de comment chainer une liste pour avoir une File](https://mooc-forums.inria.fr/moocnsi/t/autour-de-lexo2-bns-ep-6-question-sur-defile/3551/10)

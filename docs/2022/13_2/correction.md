```python linenums='1'
class Maillon:

    def __init__(self, v): 
        self.valeur = v 
        self.suivant = None

class File:

    def __init__(self):
        self.tete = None
        self.queue = None 
        
    def est_vide(self):
        return self.queue is None
    
    def affiche(self):
        maillon = self.tete 
        while maillon is not None:
            print(maillon.valeur) 
            maillon = maillon.suivant

    def enfile(self, element):
        nouveau_maillon = Maillon(element)
        if self.est_vide():
            self.tete = nouveau_maillon
        else:
            self.queue.suivant = nouveau_maillon
        self.queue = nouveau_maillon

    def defile(self):
        if not self.est_vide():
            resultat = self.tete.valeur
            self.tete = self.tete.suivant
            return resultat
```
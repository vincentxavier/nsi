On veut écrire une classe pour gérer une file à l’aide d’une liste chaînée. On dispose d’une
classe `Maillon` permettant la création d’un maillon de la chaîne, celui-ci étant constitué
d’une valeur et d’une référence au maillon suivant de la chaîne (à sa création le maillon ne connaît pas son suivant, d'où l'utilisation de l'objet `None`):

```python linenums='1'
class Maillon:
    def __init__(self, v) :
        self.valeur = v
        self.suivant = None
```

La File est constituée de maillons avec un maillon de `tete`, celui qu'on a enlever lors de l'action `defile` et un maillon de `queue` qui est le dernier à être arrivé par l'action `enfile`.

Compléter la classe `File` et vérifier votre travail sur les exemples. On vous donne l'initialiseur, la méthode `est_vide`  qui renvoie `True` si la file est vide, `False` sinon, la méthode `affiche` qui permet de voir l'état de la file manipulée. 

```python
class File:

    def __init__(self):
        self.tete = None
        self.queue = None 
        
    def est_vide(self):
        return self.queue is None
    
    def affiche(self):
        maillon = self.tete 
        print('<-- sens de la file <--')
        while maillon is not None:
            print(maillon.valeur, end=' ') 
            maillon = maillon.suivant
        print()

    def enfile(self, element):
        nouveau_maillon = ...
        if self.est_vide():
            self.tete = ...
        else:
            self.queue.suivant = ...
        self... = nouveau_maillon

    def defile(self):
        if ...:
            resultat = ...
            self.tete = ...
            return ...
```

```python
>>> F = File()
>>> F.est_vide()
True
>>> F.enfile(2)
>>> F.affiche()
<-- sens de la file <--
2
>>> F.est_vide()
False
>>> F.enfile(5)
>>> F.enfile(7)
>>> F.affiche()
<-- sens de la file <--
2 5 7
>>> F.defile()
2
>>> F.defile()
5
>>> F.affiche()
<-- sens de la file <--
7
```
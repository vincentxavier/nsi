Romain Janvier qui a relu ce sujet nous livre son verdict :

- Je ne sais pas si la figure aide vraiment. Surtout que dans l’algo, on doit s’arrêter quand le quotient est 0 et dans la figure, on s’arrête lorsque a=1. En fait le dessin correspond à cette fonction :

    ```python
    def binaire(a):
        bin_a = ""
        while a > 1:
            bin_a = str(a%2) + bin_a
            a = a // 2
        bin_a = str(a) + bin_a
        return bin_a
    ``` 

- J’ai des doutes sur le nom des variables. Je préfairais `nb` et `nb_bin`, mais là, c’est du pinaillage.
- Dans les exemples, c’est bien, il y a le cas 0 qui peut poser problème.
- Le code n’est pas très dur à remplir, même s’il n’est pas lisible par Python en l’état.
- Pour la ligne du `while`, je mettrais bien `while ...:`.

Je le rejoins essentiellement sur la figure qui n'est pas adaptée (une autre est proposée dans l'énoncé modifié).
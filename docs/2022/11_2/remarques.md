R. Janvier note des erreurs typographiques et attire l'attention sur l'utilisation de la méthode `find` pas au programme. Peut-être plutôt utiliser la fonction `ord` qui donne le code ascii d'un caractère : `ord(lettre) - ord('A')` va donner la position de `lettre` dans l'alphabet

Attention au choix des noms de variables. Dans la boucle ce sera plutôt `for caractere in message:` que `for lettre ...` car justement ce n'est pas forcément une lettre (cela peut être un signe de ponctuation).

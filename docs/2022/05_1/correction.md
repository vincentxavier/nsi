G. Connan propose la solution probablement attendue, en nous rappelant de faire attention à l'initialisation avec `None` dans le cas du tableau vide.

```python linenums='1'
def mini_et_maxi(tab):
    if len(tab) == 0:
        return  {'min': None, 'max': None}
    dic = {'min': tab[0], 'max': tab[0]}
    for nombre in tab[1:]:
        if nombre < dic['min']:
            dic['min'] = nombre
        if nombre > dic['max']:
            dic['max'] = nombre
    return dic
```

On pourrait ne pas aimer le `tab[1:]` :

```python linenums='1'
def mini_et_maxi(tab):
    if len(tab) == 0:
        return  {'min': None, 'max': None}
    dic = {'min': tab[0], 'max': tab[0]}
    for i in range(1, len(tab)):
        nombre = tab[i]
        if nombre < dic['min']:
            dic['min'] = nombre
        if nombre > dic['max']:
            dic['max'] = nombre
    return dic
```

Cela alourdit le code... alors qu'en fait on pourrait commencer au premier élément :

```python linenums='1'
def mini_et_maxi(tab):
    if len(tab) == 0:
        return  {'min': None, 'max': None}
    dic = {'min': tab[0], 'max': tab[0]}
    for nombre in tab:
        if nombre < dic['min']:
            dic['min'] = nombre
        if nombre > dic['max']:
            dic['max'] = nombre
    return dic
```

Le sujet ne précisant pas de ne pas utiliser les fonctions prédéfinies du langage ;-) :

```python linenums='1'
def mini_et_maxi(tab):
    if len(tab) == 0:
        return  {'min': None, 'max': None}
    return {'min': min(tab), 'max': max(tab)}
```

L'opérateur ternaire ? ;-) (est-il utile de dire qu'on est totalement **hors-programme** ?)

```python linenums='1'
def mini_et_maxi(tab):
    mini, maxi = min(tab), max(tab) if tab else None, None
    return {'min': mini, 'max': maxi}
```




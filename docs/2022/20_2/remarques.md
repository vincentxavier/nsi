Comme Q. Konieczko le fait remarquer : encore une erreur de paramètre nommé sur un mutable. Ajouter à cela des exemples de carrés non fournis, qui obligera les élèves à les saisir et ce sujet mérite sa :x:

Source : [commentaires à propos du 20.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/12)

Je rajouterai qu'une fonction qui retourne des types différents en fonction des données est problématique. L'exercice doit se contenter d'une fonction booléenne. On peut aussi demander au candidat de faire les appels pour créer les instances de carrés de la figure. On lui fournit les tableaux de valeurs.

La méthode d'affichage n'est jamais utilisé et peut être retirée : le sujet est déjà bien long.
Q. Konieczko trouve cet exerice 1 délicat : mal gérer le cas des négatifs ou ne pas être à l'aise avec le `range(debut, fin, pas)` peut mettre le candidat en difficulté.

Source : [commentaires à propos du 19.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/12)

Peut-être l'énoncé pourrait-il proposé une petite indication :


On rappelle que si $a$ est un entier négatif et $b$ un entier alors on peut manipuler $-a$ qui sera positif et remplacer $a \times b$ par $-(-a \times b)$.
La version attendue probablement :

```python
def nombre_de_mots(phrase):
    nb_espaces = 0
    for lettre in phrase:
        if lettre == ' ':
            nb_espaces += 1
    if phrase[len(phrase)-1] == '.':
        nb_mots = nb_espaces + 1
    else:
        nb_mots = nb_espaces
    return nb_mots
```

On peut exploiter le fait qu'à la sortie de la boucle `lettre` référence bien la dernière lettre de `phrase` et s'autoriser deux `return` pour raccourcir le code :

```python
def nombre_de_mots(phrase):
    nb_espaces = 0
    for lettre in phrase:
        if lettre == ' ':
            nb_espaces += 1
    if lettre == '.':
        return nb_espaces + 1
    else:
        return nb_espaces
```

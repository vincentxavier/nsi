
Version classique

```python linenums='1'
def recherche_positions(elt, tab):
    tab_indices = []
    for i in range(len(tab)):
        if tab[i] == elt:
            tab_indices.append(i)
    return tab_indices        
```

Version en compréhension de liste

```python linenums='1'
def recherche_positions(elt, tab):
    return [i for i in range(len(tab)) if tab[i] == elt]
```

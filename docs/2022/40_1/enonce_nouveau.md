Écrire une fonction `recherche_positions` qui prend en paramètres `elt` un nombre et `tab` un
tableau de nombres, et qui renvoie le tableau des indices de `elt` dans `tab` et le tableau vide `[]` si `elt` n'apparaît pas dans `tab`.

Exemples :
```python
>>> recherche_positions(3, [3, 2, 1, 3, 2, 1])
[0, 3]
>>> recherche_positions(4, [1, 2, 3])
[]
```
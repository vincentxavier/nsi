F. Chambon relève quelques imprécisions dans la rédaction : [commentaires à propos du 09.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/14).

Comme relevé dans d'autres sujets, les candidats n'ont pas forcément suivi une spécialité mathématiques et donc une suite récurrente avec un formalisme de mathématiques peut poser quelques difficultés.

Le _on admet_ pour un problème ouvert de plus de 90 ans est un peu osé, on peut trouver une formulation plus neutre.
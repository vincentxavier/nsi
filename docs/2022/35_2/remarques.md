R. Janvier propose :

> Problème dans la sortie. Soit un boolén, soit un couple (booléen, entier). D'ailleurs, le type de la fonction n'est pas donné dans le Docstring comme dans l'exerice 1... 
À la rigueur, on peut renvoyer `(True, 0)` quand on trouve la valeur. Ou `(True, indice)` pour indiquer la position où on a trouvé la valeur. 
Mais en fait, on complexifie juste la fonction pour rien du tout. Quel intérêt de distinguer le cas vide ou le cas où la valeur est trop petite ou trop grande ? 
Si on veut complexifier la dichotomie, on pourrait regarder le nombre d'essais faits. Mais on ne pourrait pas attendre de voir comment s'en sortent les élèves avant de rajouter des variations arbitraires ? 
Il manque des espaces après les virgules.

[commentaire à propos du 35.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62#exercice-2-dichotomie-19)

```python linenums='1'
def valeur_et_indice_max(tab):
    maximum = tab[0]
    pos_max = 0
    for i in range(len(tab)):
        if tab[i] > maximum:
            maximum = tab[i]
            pos_max = i
    return maximum, pos_max
```

Comme aucune consigne n'existe concernant les fonctions prédéfinies...

```python linenums='1'
def valeur_et_indice_max(tab):
    maximum = max(tab)
    return maximum, tab.index(maximum)
```

Une version avec `enumerate` (**hors-programme**) :

```python linenums='1'
def valeur_et_indice_max(tab):
    maximum = tab[0]
    pos_max = 0
    for position, nombre in enumerate(tab):
        if nombre > maximum:
            maximum = nombre
            pos_max = position
    return maximum, pos_max
```

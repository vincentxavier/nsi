F. Chambon relève des erreurs d'orthographe et de typographie dans l'exercice 10.1 ; ainsi qu'une fin d'énoncé peu claire. 

Source : [commentaires à propos du 10.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/14).

Après vérification, le terme _occurrence_ vient de la linguistique :

> En linguistique, l'occurrence d'un mot est son apparition dans un corpus. Pour dater l'apparition d'un mot dans une langue, les lexicographes cherchent la première occurrence de ce mot : on dit que le mot est attesté à telle ou telle date.

Source : [wikipédia](https://fr.wikipedia.org/wiki/Occurrence)

La définition donnée est donc fausse ou en tout cas peu connue.
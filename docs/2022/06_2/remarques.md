F. Chambon trouve le sujet hors-programme (recherche textuelle) et la solution à compléter proposée inutilement compliquée. Il est vrai que la double boucle `while` n'est pas très avenante.

Il milite aussi pour des noms différents pour les variables et la fonction.

On pourrait essayer de simplifier la fonction principale en la coupant en deux.
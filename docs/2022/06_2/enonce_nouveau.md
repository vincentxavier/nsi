Le fonction `coincide_a_partir_de` prend en paramètres :

- une première chaîne de caractères `chaine` 
- un indice `position` qui représente une position dans la chaîne
- une deuxième chaîne `extrait`

et qui renvoie `True` si à partir de la position `position` on retrouve la chaîne `extrait` dans `chaine`.

```python
>>> coincide_a_partir_de("ananas", 0, "an")
True
>>> coincide_a_partir_de("ananas", 2, "an")
True
>>> coincide_a_partir_de("ananas", 4, "an")
False
>>> coincide_a_partir_de("GTACAAATCTTGCC", 5, "AATC")
True
```

La fonction `est_inclus` prend en paramètres deux chaines de caractères `gene` et
`brin_adn` et renvoie `True` si on retrouve `gene` dans `brin_adn` et `False` sinon.

L'implémentation de la fonction `est_inclus` ci-dessous utilise la fonction `coincide_a_partir_de`.
Compléter les deux fonctions et vérifier avec les exemples proposés.

```python linenums='1'
def coincide_a_partir_de(chaine, position, extrait):
    if len(extrait) > len(chaine) - position:
        return False
    for i in range(len(extrait)):
        if chaine[position + ...] != ...:
            return ...
    return True

def est_inclus(gene, brin_adn):
    n = len(seq_adn)
    g = len(gene)
    i = ...
    for i in range(n - g + 1):
        if coincide_a_partir_de(..., ..., ...):
            ...
    return ...
```

Exemples :
```python
>>> est_inclus("AATC", "GTACAAATCTTGCC")
True
>>> est_inclus("AGTC", "GTACAAATCTTGCC")
False
```
Programmer la fonction `moyenne` prenant en paramètre un tableau non vide d'entiers `tableau` (type
`list`) qui renvoie la moyenne de ses éléments.

Exemples :
```python
>>> moyenne([5, 3, 8])
5.333333333333333
>>> moyenne([1, 2, 3, 4, 5, 6, 7])
4.0
```
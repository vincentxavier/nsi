```python
def moyenne(tableau):
    nb_elements, total = 0, 0
    for element in tableau:
        nb_elements += 1
        total += element
    return total / nb_elements
```

Si on s'autorise `len` :

```python linenums='1'
def moyenne(tableau):
    total = 0
    for element in tableau:
        total += element
    return total / len(tableau)
```

Avec `sum` :

```python
def moyenne(tableau):
    return sum(tableau) / len(tableau)
```


F. Chambon rappelle cette distinction assez répandue en NSI : en structures de données, le tableau est statique (on ne retire ni on n'ajoute d'élément) quand la liste est dynamique. Et cela est indépendant de l'objet `list` de Python qui permet d'implémenter les deux structures.

Ainsi l'énoncé commet une erreur en disant qu'on insère dans un tableau.

Source : [commentaires à propos du 08.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/22)

Sur les exemples on peut remplacer le deuxième qui est similaire au premier par un exemple où l'insertion se fait en fin de liste. Et ajouter un exemple avec insertion dans la liste vide.

Et les traditionnelles légèretés avec le PEP8 et les espaces.


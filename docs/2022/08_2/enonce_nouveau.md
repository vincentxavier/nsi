On considère la fonction `insere` ci-dessous qui prend en argument un entier `a_inserer` et une
liste `liste` d'entiers triés par ordre croissant. Cette fonction crée une nouvelle liste à partir de la liste `liste` en y insèrant la valeur `a_inserer` à la bonne position (la liste résultante reste triée). La fonction renvoie la nouvelle liste. Compléter la fonction `insere` :

```python linenums='1'
def insere(a_inserer, liste):
    nouvelle_liste = liste.copy() # permet d'obtenir une copie de liste
    nouvelle_liste.append(a_inserer)
    i = ...
    while a_inserer < ... and i >= ...:
        l[i+1] = ...
        l[i] = a_inserer
        i = ...
    return nouvelle_liste
```


Exemples :
```python
>>> insere(3, [1, 2, 4, 5])
[1, 2, 3, 4, 5]
>>> insere(10, [1, 2, 7])
[1, 2, 7, 10]
>>> insere(1, [2, 3, 4])
[1, 2, 3, 4]
>> insere(1, [])
[1]
```